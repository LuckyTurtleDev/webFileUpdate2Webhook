
FROM debian:stable-slim

LABEL org.opencontainers.image.url="https://gitlab.com/Lukas1818/webFileUpdate2Webhook/container_registry"
LABEL org.opencontainers.image.title="webFileUpdate2Webhook will send a webhook if a web file is updated"
LABEL org.opencontainers.image.source="https://gitlab.com/Lukas1818/webFileUpdate2Webhook"


ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
 && apt-get install -y curl sed

RUN useradd -m -u 1000 user

COPY executeList.sh /home/user/executeList.sh
COPY webFileUpdate2Webhook.sh /home/user/webFileUpdate2Webhook.sh
RUN chmod 755 /home/user/webFileUpdate2Webhook.sh /home/user/executeList.sh \
 && chown 1000:1000 /home/user/webFileUpdate2Webhook.sh /home/user/executeList.sh

USER user
WORKDIR /home/user

ENV INTERVAL=3600

CMD ["/home/user/executeList.sh"]
